using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Testing;
using Xunit;

namespace WebAPI.IntegrationTests
{
    public class SelectControllerTests: IClassFixture<WebAPIFactory<Startup>>
     {
        private readonly WebAPIFactory<Startup> _factory;
        private readonly HttpClient _client;
        public SelectControllerTests(WebAPIFactory<Startup> factory)
        {
            _factory = factory;
            _client = factory.CreateClient(new WebApplicationFactoryClientOptions
            {
                AllowAutoRedirect = false,
                BaseAddress = new Uri("https://localhost:5001")
            });
        }
   
        [Fact]
        public async Task Get_FinishedIn2021_WhenValidId_ReturnsOK(){
            //Arrage
            int id = 20;
            // Act
            var response = await _client.GetAsync($"/api/select/2?id={id}");
            //Assert
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
        }
        [Fact]
        public async Task Get_FinishedIn2021_WhenInValidId_ReturnsBadRequest(){
            //Arrage 
            int id = -123;
            //Act
            var response = await _client.GetAsync($"/api/select/2?id={id}");
            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }
    }
}