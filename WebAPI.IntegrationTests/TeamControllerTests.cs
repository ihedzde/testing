using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Common.DTO.Team;
using Microsoft.AspNetCore.Mvc.Testing;
using Newtonsoft.Json;
using Xunit;
namespace WebAPI.IntegrationTests
{
    public class TeamControllerTests: IClassFixture<WebAPIFactory<Startup>>
    {
        
        private readonly WebAPIFactory<Startup> _factory;
        private readonly HttpClient _client;
        public TeamControllerTests(WebAPIFactory<Startup> factory)
        {
            _factory = factory;
            _client = factory.CreateClient(new WebApplicationFactoryClientOptions{
                AllowAutoRedirect = false,
                BaseAddress = new Uri("https://localhost:5001")
            });
        }
        [Fact]
        public async Task Post_CreateTeam_WhenValidTeam_ThanReturnsOk()
        {
        //Arrange 
        CreateTeamDTO createTeamDTO = new CreateTeamDTO{
            Name = "Integration test are easier",
        };
        //Act
        var response = await _client.PostAsync("/api/team", new StringContent(
            JsonConvert.SerializeObject(createTeamDTO), Encoding.UTF8,
            "application/json"
        ));
        //Assert
        Assert.Equal(HttpStatusCode.OK, response.StatusCode);
        Assert.NotNull(JsonConvert.DeserializeObject<TeamDTO>(await response.Content.ReadAsStringAsync()).Id);
        }
        [Fact]
        public async Task Post_CreateTeam_WhenInValidTeam_ThanReturnsBadRequest()
        {
        //Arrange 
        CreateTeamDTO createTeamDTO = new CreateTeamDTO{
            Name = null,
        };
        //Act
        var response = await _client.PostAsync("/api/team", new StringContent(
            JsonConvert.SerializeObject(createTeamDTO), Encoding.UTF8,
            "application/json"
        ));
        //Assert
        Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }
    
    }
}