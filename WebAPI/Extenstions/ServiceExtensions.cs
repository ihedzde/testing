using System.Collections.Generic;
using System.Reflection;
using BLL.Domain.Services;
using BLL.MappingProfiles;
using BLL.Services;
using DAL.Domain.Models;
using DAL.Domain.Repositories;
using DAL.Persistance.Context;
using DAL.Persistance.Repositories;
using Microsoft.Extensions.DependencyInjection;

namespace WebAPI.Extenstions
{
    public static class ServiceExtensions
    {
        public static void RegisterCustomServices(this IServiceCollection services)
        {
            services.AddScoped<AppDbContext>();
            services.AddScoped<IRepository<UserModel>, UserRepository>();
            services.AddScoped<IRepository<TeamModel>, TeamRepository>();
            services.AddScoped<IRepository<TaskModel>, TaskRepository>();
            services.AddScoped<IRepository<ProjectModel>, ProjectRepository>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<ITeamService, TeamService>();
            services.AddScoped<ITaskService, TaskService>();
            services.AddScoped<IProjectService, ProjectService>();
            services.AddScoped<IAggregateServices, AggregateServices>();
        }

        public static void RegisterAutoMapper(this IServiceCollection services)
        {
            services.AddAutoMapper(cfg =>
            {
                cfg.AddProfile<UserProfile>();
            },
            Assembly.GetExecutingAssembly());
        }
    }
}