﻿
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using BLL.Services;
using Common.DTO.Selects;
using System.Collections.Generic;
using System.Linq;
using Common.DTO.Task;
using BLL.Domain.Services;
using System;

namespace WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class SelectController : ControllerBase
    {
        private readonly IAggregateServices _aggregateServices;
        public SelectController(IAggregateServices selectServices)
        {
            _aggregateServices = selectServices;
        }
        [HttpGet("0")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public ActionResult<Dictionary<string, int>> GetTasksEntitiesById(int id)
        {
            return Ok(_aggregateServices.GetTasksEntitiesById(id));
        }
        [HttpGet("1")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public IActionResult GetLessThan45(int id)
        {
            return Ok(_aggregateServices.GetLessThan45(id));
        }
        [HttpGet("2")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public ActionResult<IList<IdAndName>> FinishedIn2021(int id)
        {
            try
            {
                return Ok(_aggregateServices.FinishedIn2021(id));
            }catch(ArgumentException e){
                return BadRequest(e.Message);
            }
        }
        [HttpGet("3")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public ActionResult<IList<IGrouping<string, IdTeamNameMembers>>> TeamsOlder10Sorted()
        {
            return Ok(_aggregateServices.TeamsOlder10Sorted());
        }
        [HttpGet("4")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public ActionResult<IList<IdUserAndTasks>> UsersAlphabeticByTasksLength()
        {
            return Ok(_aggregateServices.UsersAlphabeticByTasksLength());
        }
        [HttpGet("5")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        public ActionResult<UserTaskCountUnfinishedTaskCountLongestTask> ComplexUserEntity(int id)
        {
            return Ok(_aggregateServices.ComplexUserEntity(id));
        }
        [HttpGet("6")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public ActionResult<IList<ProjectTaskUserCountMix>> OddlyProjectTaskUserCountMix()
        {
            return Ok(_aggregateServices.OddlyProjectTaskUserCountMix());
        }
    }
}
