﻿using System;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using BLL.Services;
using Common.DTO.Team;
using System.Net;
using System.Net.Http;
using WebAPI.CustomHttpResponse;
using BLL.Domain.Services;

namespace WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TeamController : ControllerBase
    {
        private readonly ITeamService _teamService;
        public TeamController(ITeamService teamService)
        {
            _teamService = teamService;
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public IActionResult GetAllTeams()
        {
            return Ok(_teamService.GetAllTeams());
        }
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult CreateTeam(CreateTeamDTO team)
        {
            if (!ModelState.IsValid)
                return BadRequest("Model is invalid.");
            return Ok(_teamService.CreateTeam(team));
        }
        [HttpPut]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult UpdateTeam(TeamDTO team)
        {
            if (!ModelState.IsValid)
                return BadRequest("Model is invalid.");
            _teamService.UpdateTeam(team);
            return NoContent();
        }
        [HttpDelete()]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status405MethodNotAllowed)]
        public IActionResult DeleteTeam(int id)
        {
            try
            {
                _teamService.DeleteTeam(id);
            }
            catch (InvalidOperationException e)
            {
                var response = new HttpResponseMessage(HttpStatusCode.MethodNotAllowed);
                response.Content = new StringContent(e.Message);
                return new HttpResponseMessageResult(response);//Status405MethodNotAllowed isn't implemented by default.
            }
            return NoContent();
        }
    }
}
