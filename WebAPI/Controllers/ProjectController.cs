﻿using System;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using BLL.Services;
using Common.DTO.Project;
using System.Net;
using System.Net.Http;
using WebAPI.CustomHttpResponse;
using BLL.Domain.Services;

namespace WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ProjectController : ControllerBase
    {
        private readonly IProjectService _projectService;
        public ProjectController(IProjectService projectService)
        {
            _projectService = projectService;
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public IActionResult GetAllProjects()
        {
            return Ok(_projectService.GetAllProjects());
        }
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult CreateProject(CreateProjectDTO project)
        {
            if (!ModelState.IsValid)
                return BadRequest("Model is invalid.");
            try
            {
                return Ok(_projectService.CreateProject(project));
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }
        }
        [HttpPut]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult UpdateProject(ProjectDTO project)
        {
            if (!ModelState.IsValid)
                return BadRequest("Model is invalid.");
            _projectService.UpdateProject(project);
            return NoContent();
        }
        [HttpDelete()]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        public IActionResult DeleteProject(int id)
        {
            _projectService.DeleteProject(id);
            return NoContent();
        }
    }
}
