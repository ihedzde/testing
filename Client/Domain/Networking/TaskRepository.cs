using Newtonsoft.Json;
using Client.Domain.Configs;
using System;
using System.Net.Http;
using System.Threading.Tasks;
using System.Collections.Generic;
using Client.Domain.Interfaces;
using Client.Domain.Models;
using System.Linq;
using Common.DTO.Task;
using System.Text;

namespace Client.Domain.Networking
{
    public class TaskRepository : IDisposable, IRepository<TaskModel, CreateTaskDTO>
    {
        private readonly HttpClient _client;
        private readonly string _route = Routes.Task.ToString();

        public TaskRepository()
        {
            _client = new HttpClient();
            _client.BaseAddress = new Uri(Config.FetchUrlBase);
        }
        public void Dispose()
        {
            _client.Dispose();
        }

        public async Task<TaskModel> Create(CreateTaskDTO data)
        {
            var response = await _client.PostAsync(_route, new StringContent(JsonConvert.SerializeObject(data), Encoding.UTF8, "application/json"));
            return JsonConvert.DeserializeObject<TaskModel>(await response.Content.ReadAsStringAsync());
        }
        public async Task<IList<TaskModel>> Read()
        {
            var json = (await _client.GetAsync(_route)).Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<IList<TaskModel>>(await json);
        }

        public async Task Update(TaskModel data)
        {
            await _client.PutAsync(_route, new StringContent(JsonConvert.SerializeObject(data), Encoding.UTF8, "application/json"));
        }
        public async Task Delete(int id)
        {
            await _client.DeleteAsync(_route + $"?id={id}");
        }
    }
}
