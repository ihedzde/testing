using Newtonsoft.Json;
using Client.Domain.Configs;
using System;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Collections.Generic;
using Client.Domain.Interfaces;
using Client.Domain.Models;
using Common.DTO.Team;
using System.Text;

namespace Client.Domain.Networking
{
    public class TeamRepository : IDisposable, IRepository<TeamModel, CreateTeamDTO>
    {
        private readonly HttpClient _client;

        private readonly string _route = Routes.Team.ToString();

        public TeamRepository()
        {
            _client = new HttpClient();
            _client.BaseAddress = new Uri(Config.FetchUrlBase);
        }
        public void Dispose()
        {
            _client.Dispose();
        }

        public async Task<TeamModel> Create(CreateTeamDTO data)
        {
            var response = await _client.PostAsync(_route, new StringContent(JsonConvert.SerializeObject(data), Encoding.UTF8, "application/json"));
            return JsonConvert.DeserializeObject<TeamModel>(await response.Content.ReadAsStringAsync());
        }

        public async Task<IList<TeamModel>> Read()
        {
            var json = (await _client.GetAsync(_route)).Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<IList<TeamModel>>(await json);
        }

        public async Task Update(TeamModel data)
        {
            await _client.PutAsync(_route, new StringContent(JsonConvert.SerializeObject(data), Encoding.UTF8, "application/json"));
        }

        public async Task Delete(int id)
        {
            await _client.DeleteAsync(_route + $"?id={id}");

        }
    }
}
