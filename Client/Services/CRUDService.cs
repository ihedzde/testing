using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Client.Domain.Interfaces;
using Client.Domain.Models;
using Common.DTO.Project;
using Common.DTO.Task;
using Common.DTO.Team;
using Common.DTO.User;

namespace Client.Services
{
    public class CRUDService
    {
        private readonly IRepository<UserModel, CreateUserDTO> _userRepo;
        private readonly IRepository<TaskModel, CreateTaskDTO> _taskRepo;
        private readonly IRepository<ProjectModel, CreateProjectDTO> _projectRepo;
        private readonly IRepository<TeamModel, CreateTeamDTO> _teamRepo;
        public CRUDService(IRepository<UserModel, CreateUserDTO> userRepo,
          IRepository<TaskModel, CreateTaskDTO> taskRepo,
          IRepository<ProjectModel, CreateProjectDTO> projectRepo,
          IRepository<TeamModel, CreateTeamDTO> teamRepo)
        {
            _projectRepo = projectRepo;
            _taskRepo = taskRepo;
            _userRepo = userRepo;
            _teamRepo = teamRepo;
        }
        public async Task<UserModel> CreateUser()
        {
            Console.WriteLine("FirstName:");
            string firstName = Console.ReadLine();
            Console.WriteLine("LastName:");
            string lastName = Console.ReadLine();
            Console.WriteLine("Email:");
            string email = Console.ReadLine();
            Console.WriteLine("TeamId:");
            int teamId = int.Parse(Console.ReadLine());
            Console.WriteLine("Birthday:");
            DateTime birthday = DateTime.Parse(Console.ReadLine());

            var user = new CreateUserDTO
            {
                FirstName = firstName,
                LastName = lastName,
                BirthDay = birthday,
                Email = email,
                TeamId = teamId
            };
            return await _userRepo.Create(user);
        }
        public async Task<IList<UserModel>> ReadUser()
        {
            return await _userRepo.Read();
        }
        public async Task UpdateUser()
        {
            Console.WriteLine("UserId:");
            int userId = int.Parse(Console.ReadLine());
            Console.WriteLine("New FirstName:");
            string firstName = Console.ReadLine();
            Console.WriteLine("New LastName:");
            string lastName = Console.ReadLine();
            Console.WriteLine("New Email:");
            string email = Console.ReadLine();
            Console.WriteLine("NewTeamId:");
            int teamId = int.Parse(Console.ReadLine());
            Console.WriteLine("New Birthday:");
            DateTime birthday = DateTime.Parse(Console.ReadLine());

            var user = new UserModel
            {
                Id = userId,
                FirstName = firstName,
                LastName = lastName,
                BirthDay = birthday,
                Email = email,
                TeamId = teamId
            };
            await _userRepo.Update(user);
        }
        public async Task DeleteUser()
        {
            Console.WriteLine("UserId:");
            int userId = int.Parse(Console.ReadLine());
            await _userRepo.Delete(userId);
        }
        public async Task<ProjectModel> CreateProject()
        {
            Console.WriteLine("FirstName:");
            string firstName = Console.ReadLine();
            Console.WriteLine("Name:");
            string name = Console.ReadLine();
            Console.WriteLine("Description:");
            string description = Console.ReadLine();
            Console.WriteLine("TeamId:");
            int teamId = int.Parse(Console.ReadLine());
            Console.WriteLine("AuthorId:");
            int authorId = int.Parse(Console.ReadLine());
            Console.WriteLine("Deadline:");
            DateTime deadline = DateTime.Parse(Console.ReadLine());

            var project = new CreateProjectDTO
            {
                AuthorId = authorId,
                TeamId = teamId,
                Name = name,
                Description = description,
                Deadline = deadline
            };
            return await _projectRepo.Create(project);
        }
        public async Task<IList<ProjectModel>> ReadProject()
        {
            return await _projectRepo.Read();
        }
        public async Task UpdateProject()
        {
            Console.WriteLine("ProjectId:");
            int projectId = int.Parse(Console.ReadLine());
            Console.WriteLine("FirstName:");
            string firstName = Console.ReadLine();
            Console.WriteLine("Name:");
            string name = Console.ReadLine();
            Console.WriteLine("Description:");
            string description = Console.ReadLine();
            Console.WriteLine("TeamId:");
            int teamId = int.Parse(Console.ReadLine());
            Console.WriteLine("AuthorId:");
            int authorId = int.Parse(Console.ReadLine());
            Console.WriteLine("Deadline:");
            DateTime deadline = DateTime.Parse(Console.ReadLine());

            var project = new ProjectModel
            {
                Id = projectId,
                AuthorId = authorId,
                TeamId = teamId,
                Name = name,
                Description = description,
                Deadline = deadline
            };
            await _projectRepo.Update(project);
        }
        public async Task DeleteProject()
        {
            Console.WriteLine("ProjectId:");
            int projectId = int.Parse(Console.ReadLine());
            await _projectRepo.Delete(projectId);
        }
        public async Task<TaskModel> CreateTask()
        {
            Console.WriteLine("ProjectId:");
            int projectId = int.Parse(Console.ReadLine());
            Console.WriteLine("PerformerId:");
            int performerId = int.Parse(Console.ReadLine());
            Console.WriteLine("Name:");
            string name = Console.ReadLine();
            Console.WriteLine("Description:");
            string description = Console.ReadLine();
            Console.WriteLine("TeamId:");
            int teamId = int.Parse(Console.ReadLine());
            Console.WriteLine("Choose state:\n"
            + " 0 - Development"
            + " 1 - Testing"
            + " 2 - Finished"
            + " 3 - Backlog"
            );
            TaskState state = (TaskState)Enum.Parse(typeof(TaskState), Console.ReadLine());

            var task = new CreateTaskDTO
            {
                ProjectId = projectId,
                PerformerId = performerId,
                Name = name,
                Description = description,
                State = state
            };
            return await _taskRepo.Create(task);
        }
        public async Task<IList<TeamModel>> ReadTeam()
        {
            return await _teamRepo.Read();
        }
        public async Task UpdateTask()
        {
            Console.WriteLine("TaskId:");
            int taskId = int.Parse(Console.ReadLine());
            Console.WriteLine("PerformerId:");
            int performerId = int.Parse(Console.ReadLine());
            Console.WriteLine("ProjectID:");
            int projectID = int.Parse(Console.ReadLine());
            Console.WriteLine("New Name:");
            string name = Console.ReadLine();
            Console.WriteLine("New Description:");
            string description = Console.ReadLine();
            Console.WriteLine("New Email:");
            string email = Console.ReadLine();
            Console.WriteLine("NewTeamId:");
            int teamId = int.Parse(Console.ReadLine());
            Console.WriteLine("Finished date");
            DateTime finishedAt = DateTime.Parse(Console.ReadLine());

            var task = new TaskModel
            {
                Id = taskId,
                ProjectId = projectID,
                PerformerId = performerId,
                Name = name,
                Description = description,
                FinishedAt = finishedAt,
            };
            await _taskRepo.Update(task);
        }
        public async Task DeleteTask()
        {
            Console.WriteLine("TaskId:");
            int taskId = int.Parse(Console.ReadLine());
            await _taskRepo.Delete(taskId);
        }
        
        public async Task<TeamModel> CreateTeam(){

            Console.WriteLine("Name:");
            string name = Console.ReadLine();
            var team = new CreateTeamDTO
            {
               Name = name,
            };
            return await _teamRepo.Create(team);
        }
        public async Task<IList<TaskModel>> ReadTask()
        {
            return await _taskRepo.Read();
        }
        public async Task UpdateTeam()
        {
            Console.WriteLine("TeamId:");
            int teamId = int.Parse(Console.ReadLine());
            Console.WriteLine("Name:");
            string name = Console.ReadLine();

            var team = new TeamModel
            {
                Id = teamId,
                Name = name
            };
            await _teamRepo.Update(team);
        }
        public async Task DeleteTeam()
        {
            Console.WriteLine("TeamId:");
            int teamId = int.Parse(Console.ReadLine());
            await _teamRepo.Delete(teamId);
        }
    }
}