using System;
using System.Linq;
using System.Collections.Generic;
using Common.DTO.Team;
using DAL.Domain.Repositories;
using AutoMapper;
using Common.DTO.User;
using DAL.Domain.Models;
using BLL.Domain.Services;

namespace BLL.Services
{
    public class TeamService: ITeamService
    {
        private readonly IRepository<TeamModel> _teamRepo;
        private readonly IRepository<UserModel> _userRepo;
        private readonly IRepository<ProjectModel> _projectRepo;
        private readonly IMapper _mapper;
        public TeamService(IMapper mapper, IRepository<TeamModel> teamRepo,
         IRepository<UserModel> userRepo, IRepository<ProjectModel> projectRepo)
        {
            _teamRepo = teamRepo;
            _userRepo = userRepo;
             _projectRepo = projectRepo;
            _mapper = mapper;
        }
        private TeamDTO ModelToTeamDTO(TeamModel model)
        {
            var members = _userRepo.ReadAll().Where(user => user.TeamId == model.Id);
            return new TeamDTO
            {
                Id = model.Id,
                Name = model.Name,
                Members = members.Select(x => _mapper.Map<UserModel, UserDTO>(x)).ToList(),
                CreatedAt = model.CreatedAt
            };
        }
        private TeamModel TeamDTOToModel(TeamDTO model)
        {
            return new TeamModel
            {
                Id = model.Id,
                Name = model.Name,
                CreatedAt = model.CreatedAt
            };
        }
        private TeamDTO CreateModelToTeamDTO(CreateTeamDTO model) =>
        new TeamDTO
        {
            Name = model.Name,
            CreatedAt = DateTime.Now
        };
        public TeamDTO GetById(int id){
            return ModelToTeamDTO(_teamRepo.Read(id));
        }
        public IEnumerable<TeamDTO> GetAllTeams()
        {
            return _teamRepo.ReadAll().Select(team => ModelToTeamDTO(team));
        }
        public TeamDTO CreateTeam(CreateTeamDTO createTeamDTO)
        {
            return ModelToTeamDTO(_teamRepo.Create(TeamDTOToModel(CreateModelToTeamDTO(createTeamDTO))));
        }
        public void UpdateTeam(TeamDTO updateDto){
            _teamRepo.Update(TeamDTOToModel(updateDto));
        }
        public void DeleteTeam(int id){
            if(_projectRepo.ReadAll().Any(project=>project.TeamId == id))
                throw new InvalidOperationException("Cannot delete Team with dependency on Project, reasign to other teams or delete.");
            _userRepo.ReadAll().Where(user=>user.TeamId == id).ToList()
            .ForEach(user=>{
                user.TeamId = null;
                _userRepo.Update(user);
            });

        }
    }
}