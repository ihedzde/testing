using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Common.DTO.User;
using DAL.Domain.Repositories;
using DAL.Domain.Models;
using BLL.Domain.Services;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace BLL.Services
{
    public class UserService : IUserService
    {
        private readonly IRepository<UserModel> _userRepo;
        private readonly IRepository<TeamModel> _teamRepo;
        private readonly IMapper _mapper;
        public UserService(IMapper mapper, IRepository<UserModel> userRepo, IRepository<TeamModel> teamRepo)
        {
            _userRepo = userRepo;
            _teamRepo = teamRepo;
            _mapper = mapper;
        }
        public IEnumerable<UserDTO> GetAllUsers()
        {
            return _userRepo.ReadAll().Select(model => _mapper.Map<UserModel, UserDTO>(model));
        }
        public UserDTO GetById(int id)
        {
            return _mapper.Map<UserModel, UserDTO>(_userRepo.Read(id));
        }
        private void ValidateCreateDTO(CreateUserDTO createUserDTO)
        {
            StringBuilder stringBuilder = new StringBuilder();
            if (string.IsNullOrEmpty(createUserDTO.FirstName))
                stringBuilder.AppendLine("FirstName can't be empty");
            if (string.IsNullOrEmpty(createUserDTO.LastName))
                stringBuilder.AppendLine("LastName can't be empty");
            if (createUserDTO.Email == null || !new EmailAddressAttribute().IsValid(createUserDTO.Email))
                stringBuilder.AppendLine("Email is invalid");
            if (createUserDTO.BirthDay > DateTime.Now)
                stringBuilder.AppendLine($"User birtday can't be earlier than today{DateTime.Now.ToString()}");
            if (createUserDTO.TeamId != null)
            {
                if (_teamRepo.Read((int)createUserDTO.TeamId) == null)
                    stringBuilder.AppendLine($"No team with such {createUserDTO.TeamId} teamId was found");
            }

            var argumentExceptionMessage = stringBuilder.ToString();
            if (argumentExceptionMessage.Length > 0)
                throw new ArgumentException(argumentExceptionMessage);

        }
        private void ValidateUpdateDTO(UserDTO updateUserDTO)
        {
            StringBuilder stringBuilder = new StringBuilder();
            if (string.IsNullOrEmpty(updateUserDTO.FirstName))
                stringBuilder.AppendLine("FirstName can't be empty");
            if (string.IsNullOrEmpty(updateUserDTO.LastName))
                stringBuilder.AppendLine("LastName can't be empty");
            if (updateUserDTO.Email == null || !new EmailAddressAttribute().IsValid(updateUserDTO.Email))
                stringBuilder.AppendLine("Email is invalid");
            if (updateUserDTO.BirthDay > DateTime.Now)
                stringBuilder.AppendLine($"User birtday can't be earlier than today{DateTime.Now.ToString()}");
            if (updateUserDTO.TeamId != null)
            {
                if (_teamRepo.Read((int)updateUserDTO.TeamId) == null)
                    stringBuilder.AppendLine($"No team with such {updateUserDTO.TeamId} teamId was found");
            }

            var argumentExceptionMessage = stringBuilder.ToString();
            if (argumentExceptionMessage.Length > 0)
                throw new ArgumentException(argumentExceptionMessage);

        }
        public UserDTO CreateUser(CreateUserDTO createUserDTO)
        {
            ValidateCreateDTO(createUserDTO);
            return _mapper.Map<UserModel, UserDTO>(_userRepo.Create(_mapper.Map<UserDTO, UserModel>(_mapper.Map<CreateUserDTO, UserDTO>(createUserDTO))));
        }
        public void DeleteUser(int id)
        {
            if(id < 0)
                throw new ArgumentException("Invalid id can't be negative");
            var model = _userRepo.Delete(id);
            if(model == null)
                throw new ArgumentException($"No such model with id {id} was found");
        }
        public void UpdateUser(UserDTO updateUser)
        {
            ValidateUpdateDTO(updateUser);
            _userRepo.Update(_mapper.Map<UserDTO, UserModel>(updateUser));
        }

    }
}