using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Common.DTO.Project;
using Common.DTO.User;
using DAL.Domain.Repositories;
using DAL.Domain.Models;
using BLL.Domain.Services;
using System.Text;

namespace BLL.Services
{
    public class ProjectService : IProjectService
    {
        private readonly IRepository<ProjectModel> _projectRepo;
        private readonly IRepository<UserModel> _userRepo;
        private readonly IRepository<TeamModel> _teamRepo;
        private readonly ITaskService _taskService;
        private readonly ITeamService _teamService;
        private readonly IMapper _mapper;
        public ProjectService(IMapper mapper, IRepository<ProjectModel> projectRepo, IRepository<UserModel> userRepo,
        IRepository<TeamModel> teamRepo,
         ITaskService taskService, ITeamService teamService)
        {
            _mapper = mapper;
            _projectRepo = projectRepo;
            _userRepo = userRepo;
            _teamRepo = teamRepo;
            _taskService = taskService;
            _teamService = teamService;
        }
        private ProjectDTO ModelToProjectDTO(ProjectModel model)
        {
            return new ProjectDTO
            {
                Id = model.Id,
                Name = model.Name,
                Description = model.Description,
                AuthorId = model.AuthorId,
                TeamId = model.TeamId,
                Author = _mapper.Map<UserDTO>(_userRepo.Read(model.AuthorId)),
                Team = _teamService.GetById(model.TeamId),
                Tasks = _taskService.GetAllTasks().Where(task => task.PerformerId == model.Id),
                CreatedAt = model.CreatedAt,
                Deadline = model.Deadline,
            };
        }
        private ProjectModel ProjectDTOToModel(ProjectDTO model)
        {
            return new ProjectModel
            {
                Id = model.Id,
                Name = model.Name,
                Description = model.Description,
                AuthorId = model.AuthorId,
                TeamId = model.TeamId,
                CreatedAt = model.CreatedAt,
                Deadline = model.Deadline
            };
        }
        private ProjectDTO CreateModelToProjectDTO(CreateProjectDTO model) =>
        new ProjectDTO
        {
            Name = model.Name,
            Description = model.Description,
            AuthorId = model.AuthorId,
            TeamId = model.TeamId,
            Author = _mapper.Map<UserModel, UserDTO>(_userRepo.Read(model.AuthorId)),
            Team = _teamService.GetById(model.TeamId),
            CreatedAt = DateTime.Now,
            Deadline = model.Deadline,
        };

        public IEnumerable<ProjectDTO> GetAllProjects()
        {
            return _projectRepo.ReadAll().Select(task => ModelToProjectDTO(task));
        }
        private void ValidateCreateDTO(CreateProjectDTO createProjectDTO)
        {
            StringBuilder stringBuilder = new StringBuilder();
            if (string.IsNullOrEmpty(createProjectDTO.Name))
                stringBuilder.AppendLine("Name can't be empty");
            if (string.IsNullOrEmpty(createProjectDTO.Description))
                stringBuilder.AppendLine("Description can't be empty");

            if (_userRepo.Read((int)createProjectDTO.AuthorId) == null)
                stringBuilder.AppendLine($"No author with such {createProjectDTO.AuthorId} authorId was found");
            if (_teamRepo.Read((int)createProjectDTO.TeamId) == null)
                stringBuilder.AppendLine($"No team with such {createProjectDTO.TeamId} teamId was found");

            var argumentExceptionMessage = stringBuilder.ToString();
            if (argumentExceptionMessage.Length > 0)
                throw new ArgumentException(argumentExceptionMessage);
        }
        public ProjectDTO CreateProject(CreateProjectDTO createProjectDTO)
        {
            ValidateCreateDTO(createProjectDTO);
            return ModelToProjectDTO(_projectRepo.Create(ProjectDTOToModel(CreateModelToProjectDTO(createProjectDTO))));
        }
        public ProjectDTO GetById(int id)
        {
            return ModelToProjectDTO(_projectRepo.Read(id));
        }
        public void UpdateProject(ProjectDTO updateDto)
        {
            _projectRepo.Update(ProjectDTOToModel(updateDto));
        }
        public void DeleteProject(int id)
        {
            _projectRepo.Delete(id);
        }

    }
}