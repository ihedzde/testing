using System;
using AutoMapper;
using Common.DTO.User;
using DAL.Domain.Models;

namespace BLL.MappingProfiles
{
    public class UserProfile: Profile
    {
        public UserProfile()
        {
            CreateMap<UserModel, UserDTO>()
            .ForMember(dest=>dest.LastName, src=> src.MapFrom(s=>s.SurName));
            CreateMap<UserDTO, UserModel>()
            .ForMember(dest=>dest.SurName, src=> src.MapFrom(s=>s.LastName));
            CreateMap<CreateUserDTO, UserDTO>()
            .ForMember(dest=>dest.RegisteredAt, src=> src.MapFrom(s=>DateTime.Now));
        }
    }
}