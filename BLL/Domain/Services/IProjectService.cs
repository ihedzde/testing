using System.Collections.Generic;
using Common.DTO.Project;

namespace BLL.Domain.Services
{
    public interface IProjectService
    {
        ProjectDTO CreateProject(CreateProjectDTO createProjectDTO);
        void DeleteProject(int id);
        IEnumerable<ProjectDTO> GetAllProjects();
        ProjectDTO GetById(int id);
        void UpdateProject(ProjectDTO updateDto);
    }

}