using System.Collections.Generic;
using Common.DTO.Team;

namespace BLL.Domain.Services
{
    public interface ITeamService
    {
        TeamDTO CreateTeam(CreateTeamDTO createTeamDTO);
        void DeleteTeam(int id);
        IEnumerable<TeamDTO> GetAllTeams();
        TeamDTO GetById(int id);
        void UpdateTeam(TeamDTO updateDto);
    }
}