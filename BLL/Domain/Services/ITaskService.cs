using System.Collections.Generic;
using Common.DTO.Task;

namespace BLL.Domain.Services
{
    public interface ITaskService
    {
        TaskDTO CreateTask(CreateTaskDTO createTaskDTO);
        void DeleteTask(int id);
        IEnumerable<TaskDTO> GetAllTasks();
        TaskDTO GetById(int id);
        TaskDTO UpdateTask(TaskDTO updateDto);
        IEnumerable<TaskDTO> GetAllUnfinishedForUser(int userId);
    }
}