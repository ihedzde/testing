using System.Collections.Generic;
using System.Linq;
using Common.DTO.Selects;
using Common.DTO.Task;

namespace BLL.Domain.Services
{
    public interface IAggregateServices
    {
        UserTaskCountUnfinishedTaskCountLongestTask ComplexUserEntity(int userId);
        IList<IdAndName> FinishedIn2021(int userId);
        IList<TaskDTO> GetLessThan45(int userId);
        Dictionary<string, int> GetTasksEntitiesById(int userId);
        IList<ProjectTaskUserCountMix> OddlyProjectTaskUserCountMix();
        IList<IGrouping<string, IdTeamNameMembers>> TeamsOlder10Sorted();
        IList<IdUserAndTasks> UsersAlphabeticByTasksLength();
    }
}