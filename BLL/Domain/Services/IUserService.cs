using System.Collections.Generic;
using Common.DTO.User;

namespace BLL.Domain.Services
{
    public interface IUserService
    {
        IEnumerable<UserDTO> GetAllUsers();
        UserDTO GetById(int id);
        UserDTO CreateUser(CreateUserDTO createUserDTO);
        void DeleteUser(int id);
        void UpdateUser(UserDTO updateUser);
    }
}