using DAL.Domain.Models;
using DAL.Persistance.Context.Seeds;
using Microsoft.EntityFrameworkCore;

namespace DAL.Persistance.Context
{
    public class AppDbContext : DbContext
    {
        public DbSet<ProjectModel> Projects { get; set; }
        public DbSet<TeamModel> Teams { get; set; }
        public DbSet<TaskModel> Tasks { get; set; }
        public DbSet<UserModel> Users { get; set; }
        public AppDbContext(DbContextOptions options) : base(options)
        {
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            #region Project
            modelBuilder.Entity<ProjectModel>().HasKey(x => x.Id);
            modelBuilder.Entity<ProjectModel>().Property(x => x.Id).IsRequired().ValueGeneratedOnAdd();
            modelBuilder.Entity<ProjectModel>().HasOne(x => x.Author);
            modelBuilder.Entity<ProjectModel>().HasOne(x => x.Team);
            modelBuilder.Entity<ProjectModel>().HasData(ProjectSeeds.Projects);
            #endregion
            #region Task
            modelBuilder.Entity<TaskModel>().HasKey(x => x.Id);
            modelBuilder.Entity<TaskModel>().Property(x => x.Id).IsRequired().ValueGeneratedOnAdd();
            modelBuilder.Entity<TaskModel>().HasOne(x => x.Project);
            modelBuilder.Entity<TeamModel>().HasMany(x => x.Members).WithOne(x => x.Team).HasForeignKey(x => x.TeamId);
            modelBuilder.Entity<TaskModel>().HasData(TaskSeeds.Tasks);
            #endregion
            #region  User
            modelBuilder.Entity<UserModel>().HasKey(x => x.Id);
            modelBuilder.Entity<UserModel>().Property(x => x.Id).IsRequired().ValueGeneratedOnAdd();
            modelBuilder.Entity<UserModel>().Property(x => x.Id).IsRequired().ValueGeneratedOnAdd();
            modelBuilder.Entity<UserModel>().HasOne(x => x.Team);
            modelBuilder.Entity<TeamModel>().HasData(TeamSeeds.Teams);
            modelBuilder.Entity<UserModel>().HasData(UserSeeds.Users);
            #endregion
            base.OnModelCreating(modelBuilder);
        }


    }
}