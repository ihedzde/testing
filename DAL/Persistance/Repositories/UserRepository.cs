using System.Linq;
using System.Collections.Generic;
using DAL.Domain.Repositories;
using System;
using DAL.Persistance.Context;
using DAL.Domain.Models;

namespace DAL.Persistance.Repositories
{
    public class UserRepository : BaseRepository, IRepository<UserModel>
    {
        public UserRepository(AppDbContext context): base(context)
        {
           
        }
        public UserModel Create(UserModel data)
        {
            dbContext.Users.Add(data);
            dbContext.SaveChanges();
            return data;
        }

        public UserModel Delete(int id)
        {
            var deleteObj = dbContext.Users.FirstOrDefault(p=>p.Id == id);
            dbContext.Users.Remove(deleteObj);
            dbContext.SaveChanges();
            return deleteObj;
        }

        public UserModel Read(int id)
        {
            return dbContext.Users.FirstOrDefault(p=>p.Id == id);
        }

        public IList<UserModel> ReadAll()
        {
            return dbContext.Users.ToList();
        }

        public UserModel Update(UserModel data)
        {
            dbContext.Users.Update(data);
            dbContext.SaveChanges();
            return data;
        }
    }
}