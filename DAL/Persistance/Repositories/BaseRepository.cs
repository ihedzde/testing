using DAL.Persistance.Context;

namespace DAL.Persistance.Repositories
{
    public abstract class BaseRepository
    {
        protected readonly AppDbContext dbContext;
        public BaseRepository(AppDbContext context)
        {
            dbContext = context;
        }
    }
}