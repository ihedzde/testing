using System.Threading.Tasks;
using System.Collections.Generic;

namespace DAL.Domain.Repositories
{
    public interface IRepository<T> where T : class
    {
        IList<T> ReadAll();
        T Create(T data);
        T Read(int id);
        T Update(T data);
        T Delete(int id);
    }
}