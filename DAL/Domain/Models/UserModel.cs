using System;
using System.ComponentModel.DataAnnotations;

namespace DAL.Domain.Models
{
    public class UserModel
    {
        public int Id { get; set; }
        public int? TeamId { get; set; }
        public TeamModel Team { get; set; }
        public string FirstName { get; set; }
        public string SurName { get; set; }
        
        [EmailAddressAttribute]
        public string Email { get; set; }
        public DateTime RegisteredAt { get; set; }
        public DateTime BirthDay { get; set; }
    }
}
