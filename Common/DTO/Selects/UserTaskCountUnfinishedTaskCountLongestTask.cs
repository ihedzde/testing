using Common.DTO.Task;
using Common.DTO.User;

namespace Common.DTO. Selects
{
    public class UserTaskCountUnfinishedTaskCountLongestTask
    {
        public UserDTO User { get; set; }

        public int TaskCount { get; set; }
        public int UnfinishedTaskCount { get; set; }

        public TaskDTO LongestTask { get; set; }

        public override string ToString()
        {
            return $"User: {User.ToString()} TaskCount: {TaskCount} UnfinishedTaskCount: {UnfinishedTaskCount} TaskDTO {LongestTask.ToString()}";
        }
    }
}