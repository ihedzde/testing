using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
namespace Common.DTO.Team
{
    public class CreateTeamDTO
    {
        [Required]
        public string Name { get; set; }
    }
}