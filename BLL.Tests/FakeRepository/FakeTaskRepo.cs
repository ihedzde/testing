using System;
using System.Linq;
using System.Collections.Generic;
using DAL.Domain.Models;
using DAL.Domain.Repositories;
using DAL.Persistance.Context.Seeds;

namespace BLL.Tests.FakeRepository
{
    public class FakeTaskRepo : IRepository<TaskModel>
    {
        private readonly IList<TaskModel> _storage;
        public FakeTaskRepo()
        {
            _storage = TaskSeeds.Tasks;
        }
        public TaskModel Create(TaskModel data)
        {
            int id = new Random().Next();
            data.Id = id;
            _storage.Add(data);
            return data;
        }

        public TaskModel Delete(int id)
        {
           var task = _storage.FirstOrDefault(x=>x.Id == id);
           _storage.Remove(task);
           return task;
        }

        public TaskModel Read(int id)
        {
            return _storage.FirstOrDefault(x=>x.Id == id);
        }

        public IList<TaskModel> ReadAll()
        {
           return _storage;
        }

        public TaskModel Update(TaskModel data)
        {
            var task = _storage.FirstOrDefault(x=>x.Id == data.Id);
            var taskIndex = _storage.IndexOf(task);
            _storage[taskIndex] = data;
            return data;
        }
    }
}