using System;
using BLL.Services;
using Common.DTO.User;
using Xunit;
using FakeItEasy;
using AutoMapper;
using DAL.Persistance.Repositories;
using BLL.MappingProfiles;
using Newtonsoft.Json;
using Common.DTO.Task;
using BLL.Tests.FakeRepository;
using DAL.Domain.Repositories;
using DAL.Domain.Models;
using DAL.Persistance.Context.Seeds;

namespace BLL.Tests
{
    public class TaskServiceTests
    {
        private readonly IRepository<UserModel> _userRepo;
        private readonly IRepository<TaskModel> _taskRepo;
        private readonly IRepository<ProjectModel> _projectRepo;
        private readonly TaskService _taskService;
        public TaskServiceTests()
        {
            var mapper = new MapperConfiguration(cfg =>
           {
               cfg.AddProfile(new UserProfile());
           }).CreateMapper();
            _userRepo = A.Fake<IRepository<UserModel>>();
            A.CallTo(() => _userRepo.ReadAll()).Returns(UserSeeds.Users);
            _taskRepo = new FakeTaskRepo();
            _projectRepo = A.Fake<IRepository<ProjectModel>>();
            A.CallTo(() => _projectRepo.ReadAll()).Returns(ProjectSeeds.Projects);
            _taskService = new TaskService(mapper, _taskRepo, _userRepo, _projectRepo);
        }
        [Fact]
        void GetAllUnfinishedForUser_WhenValidUserId_ThanReturnsList()
        {
            //Arrange
            int userId = 1;
            //Act
            var result = _taskService.GetAllUnfinishedForUser(userId);
            //Assert
            Assert.NotNull(result);
        }
          [Fact]
        void GetAllUnfinishedForUser_WhenInvalidUserId_ThanReturnsList()
        {
            //Arrange
            int userId = -123;
            //Act Assert
            Assert.Throws<ArgumentException>(()=>_taskService.GetAllUnfinishedForUser(userId));
        }
        [Fact]
        public void UpdateTask_WhenChangeStatusToFinished_ThanFinishedDateNotNull()
        {
            //Arrange
            A.CallTo(() => _userRepo.Read(1)).Returns(
                new UserModel
                {
                    Id = 1,
                    FirstName = "John",
                    SurName = "Dou",
                    RegisteredAt = new DateTime(2021, 7, 5)
                }
            );
            A.CallTo(() => _projectRepo.Read(1)).Returns(
                new ProjectModel
                {
                    Id = 1,
                    Name = "Unit testing",
                    AuthorId = 1,
                    Deadline = new DateTime(2021, 7, 6)
                });
            var model = _taskRepo.Create(new TaskModel
            {
                TLDR = "Description",
                ProjectId = 1,
                PerformerId = 1,
                CreatedAt = new DateTime(2021, 7, 5),
                State = (short)TaskState.Development,
            });
            //Act
            var userDTO = _taskService.UpdateTask(new TaskDTO
            {
                Id = model.Id,
                Name = "New Name",
                Description = "New Description",
                ProjectId = 1,
                PerformerId = 1,
                CreatedAt = new DateTime(2021, 7, 5),
                State = TaskState.Finished,
            });
            //Assert
            Assert.NotNull(userDTO.FinishedAt);
        }
        [Fact]
        public void UpdateTask_WhenChangeStatusFromFinishedToAnyOther_ThanFinishedDateNull()
        {
            //Arrange
            A.CallTo(() => _userRepo.Read(1)).Returns(
                new UserModel
                {
                    Id = 1,
                    FirstName = "John",
                    SurName = "Dou",
                    RegisteredAt = new DateTime(2021, 7, 5)
                }
            );
            A.CallTo(() => _projectRepo.Read(1)).Returns(
                new ProjectModel
                {
                    Id = 1,
                    Name = "Unit testing",
                    AuthorId = 1,
                    Deadline = new DateTime(2021, 7, 6)
                });
            var model = _taskRepo.Create(new TaskModel
            {
                TLDR = "Description",
                ProjectId = 1,
                PerformerId = 1,
                CreatedAt = new DateTime(2021, 7, 5),
                State = (short)TaskState.Finished,
            });
            //Act
            var userDTO = _taskService.UpdateTask(new TaskDTO
            {
                Id = model.Id,
                Name = "New Name",
                Description = "New Description",
                ProjectId = 1,
                PerformerId = 1,
                CreatedAt = new DateTime(2021, 7, 5),
                State = TaskState.Testing,
            });
            //Assert
            Assert.Null(userDTO.FinishedAt);
        }
    }
}