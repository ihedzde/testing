using System;
using BLL.Services;
using Common.DTO.User;
using Xunit;
using FakeItEasy;
using AutoMapper;
using BLL.MappingProfiles;
using Newtonsoft.Json;
using System.Collections.Generic;
using DAL.Domain.Repositories;
using DAL.Domain.Models;
using DAL.Persistance.Context.Seeds;

namespace BLL.Tests
{
    public class UserServiceTests
    {
        private UserService _service;
        private IRepository<UserModel> _userRepo;
        private IRepository<TeamModel> _teamRepo;
        public UserServiceTests()
        {
            var mapper = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new UserProfile());
            }).CreateMapper();
            _userRepo = A.Fake<IRepository<UserModel>>();
            _teamRepo = A.Fake<IRepository<TeamModel>>();
            A.CallTo(()=>_teamRepo.ReadAll()).Returns(TeamSeeds.Teams);
            _service = new UserService(mapper, _userRepo, _teamRepo);
        }
        public static IEnumerable<object[]> ValidUsers()
        {
            yield return new object[]{ new CreateUserDTO {
                FirstName = "John",
                LastName = "Dou",
                Email = "John.Dou@mail.com",
                BirthDay = new DateTime(2003, 12, 30)
            }};
            yield return new object[]{new CreateUserDTO {
                FirstName = "Jill",
                LastName = "Johnson",
                TeamId = 20,
                Email = "Jill.Johnson@mail.com",
                BirthDay = new DateTime(2003, 12, 30)
            }};
        }

        [Theory]
        [MemberData(nameof(ValidUsers))]
        public void CreateUser_WhenModelValid_ThanCreated(CreateUserDTO createUserDTO)
        {
            //Arrange Act
            var userDTO = _service.CreateUser(createUserDTO);
            //Assert
            A.CallTo(() => _userRepo.Create(A<UserModel>.Ignored)).MustHaveHappened();
        }
        public static IEnumerable<object[]> InvalidUsers()
        {
            yield return new object[]{ new CreateUserDTO{
                FirstName = "John",
                LastName = "Dou",
                Email = "Jon.Dou-mail.com"
            }};
            yield return new object[]{ new CreateUserDTO{
                FirstName = "",
                LastName = "Dou",
                Email = "Dou@mail.com"
            }};
            yield return new object[]{ new CreateUserDTO{
                FirstName = "John",
                LastName = "Dou",
                TeamId = 1010,
                Email = "Dou@mail.com"
            }};
            yield return new object[]{ new CreateUserDTO{
                FirstName = "",
                LastName = "",
                TeamId = 12,
                Email = "🤨"
            }};
        }
        [Theory]
        [MemberData(nameof(InvalidUsers))]
        public void CreateUser_WhenModelInvalid_ThanThrowArgumentException(CreateUserDTO createUserDTO)
        {
            //Arrange
            A.CallTo(() => _teamRepo.Read(A<int>.Ignored)).Returns(null);
            // Act Assert
            Assert.Throws<ArgumentException>(() => _service.CreateUser(createUserDTO));
        }
    }
}