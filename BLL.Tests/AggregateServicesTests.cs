using System;
using System.Linq;
using AutoMapper;
using BLL.Domain.Services;
using BLL.MappingProfiles;
using BLL.Services;
using Xunit;
using FakeItEasy;
using DAL.Domain.Repositories;
using DAL.Domain.Models;
using DAL.Persistance.Context.Seeds;

namespace BLL.Tests
{
    public class AggregateServicesTests
    {
        private readonly AggregateServices _aggregateServices;
        public AggregateServicesTests()
        {
            var mapper = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new UserProfile());
            }).CreateMapper();
            var fakeTeamRepo = A.Fake<IRepository<TeamModel>>();
            A.CallTo(() => fakeTeamRepo.ReadAll()).Returns(TeamSeeds.Teams);
            var fakeUserRepo = A.Fake<IRepository<UserModel>>();
            A.CallTo(() => fakeUserRepo.ReadAll()).Returns(UserSeeds.Users);
            var fakeTaskRepo = A.Fake<IRepository<TaskModel>>();
            A.CallTo(() => fakeTaskRepo.ReadAll()).Returns(TaskSeeds.Tasks);
            var fakeProjectRepo = A.Fake<IRepository<ProjectModel>>();
            A.CallTo(() => fakeProjectRepo.ReadAll()).Returns(ProjectSeeds.Projects);
            ITeamService teamService = new TeamService(mapper, fakeTeamRepo, fakeUserRepo, fakeProjectRepo);
            IUserService userService = new UserService(mapper, fakeUserRepo, fakeTeamRepo);
            ITaskService taskService = new TaskService(mapper, fakeTaskRepo, fakeUserRepo, fakeProjectRepo);
            IProjectService productService = new ProjectService(mapper, fakeProjectRepo, fakeUserRepo, fakeTeamRepo, taskService, teamService);
            _aggregateServices = new AggregateServices(productService, userService);
        }
        [Fact]
        public void GetTasksEntitiesById_WhenIdIsNegative_ThanThrowInvalidArgumentException()
        {
            //Arrange
            int id = -10;
            //Act Assert
            Assert.Throws<ArgumentException>(() => _aggregateServices.GetTasksEntitiesById(id));
        }
        [Fact]
        public void ComplexUserEntity_WhenIdIsNegative_ThanThrowInvalidArgumentException()
        {
            //Arrange
            int id = -110;
            //Act Assert
            Assert.Throws<ArgumentException>(() => _aggregateServices.ComplexUserEntity(id));
        }
        [Fact]
        public void GetLessThan45_WhenIdIsNegative_ThanThrowInvalidArgumentException()
        {
            //Arrange
            int id = -34;
            //Act Assert
            Assert.Throws<ArgumentException>(() => _aggregateServices.GetLessThan45(id));
        }
        [Fact]
        public void FinishedIn2021_WhenIdIsNegative_ThanThrowInvalidArgumentException()
        {
            //Arrange
            int id = -42;
            //Act Assert
            Assert.Throws<ArgumentException>(() => _aggregateServices.FinishedIn2021(id));
        }
        [Fact]
        public void TeamsOlder10Sorted_WhenCalled_ThanReturnsNotEmptyList()
        {

            Assert.NotEmpty(_aggregateServices.TeamsOlder10Sorted());
        }
        [Fact]
        public void UsersAlphabeticByTasksLength_WhenCalled_ThanReturnsNotEmptyList()
        {
            Assert.NotEmpty(_aggregateServices.UsersAlphabeticByTasksLength());
        }
        [Fact]
        public void OddlyProjectTaskUserCountMix_WhenCalled_ThanReturnsNotEmpty()
        {
            Assert.NotEmpty(_aggregateServices.OddlyProjectTaskUserCountMix());
        }
    }
}